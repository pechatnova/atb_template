$(document).ready(function(){

$('.main_menu .item a.main').on('click', function(e){
    e.preventDefault();
    $('html').removeClass('ops');
    $(this).parents('header').removeClass('active');
    $('.main_menu .item').removeClass('active');


    $('html').addClass('ops');
    $(this).parents('header').addClass('active');
    $(this).parent().addClass('active');
});

    $('.div_ops').on('click', function(){
        $('html').removeClass('ops');
        $('header').removeClass('active');
        $('.main_menu .item').removeClass('active');
    });



    $('.main_menu .item a.main').on('dblclick', function(){
        $('html').removeClass('ops');
        $('header').removeClass('active');
        $('.main_menu .item').removeClass('active');
    });




    $('.features_bank a.big').on('click', function(e){
        e.preventDefault();
        $(this).parent().toggleClass('active');
    });




    //slick
    $('.products_bank .prod_block').slick({
        arrows: false,
        dots: false,
        infinite: true,
        slidesToShow: 3,
        responsive: [
            {
                breakpoint: 1981,
                settings: "unslick"
            },
            {
                breakpoint: 1260,
                settings: "unslick"
            },
            {
                breakpoint: 990,
                settings: {
                    arrows: false,
                    dots: false,
                    slidesToShow: 3,
                    initialSlide: 0
                }
            },
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    dots: false,
                    slidesToShow: 1,
                    initialSlide: 1
                }
            }
        ]
    });




    //communication_with_us
    $('.top_menu .communic a').on('click', function(e){
        e.preventDefault();
        $('.top_menu .communic').addClass('active');
    });

    $('.top_menu .block a.close_block').on('click', function(e){
        e.preventDefault();
        $('.top_menu .item').removeClass('active');
    });



    //block slider
    $('.block_slides_ns .inn_sl').slick({
        dots: true,
        arrows: false,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 2000,
        autoplay: false,
        autoplaySpeed: 4000
    });



    //search
    $('.search a.srch').on('click', function(e){
        e.preventDefault();
        $(this).parent().addClass('active');
    });

    $('.search .block a.close_search').on('click', function(e){
        e.preventDefault();
        $(this).parents().removeClass('active');
    });



});





$(document).on('keyup', 'input[name=city_ns]', function(event){

    var city = $(this).val();
    if(city.length>1){
        city_select(city);
    }

});

$(document).on('focusout', 'input[name=city_ns]', function(event){
	$('#city_select_ns').html('');

});

function city_select(city) {
    $.ajax({
        type: "POST",
        url: "/local/templates/main/new_style/ajax/city_select.php",
        data: ({     
            "city" : city
        }),
        success: function(a){
            $('#city_select_ns').html(a);
        }
    }); 
}